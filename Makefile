ASM=nasm
ASMFLAGS=-f elf64
LD=ld

all: start
%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
start: main.o dict.o lib.o
	$(LD) -o $@ $^

clean:
	rm -rf *.o start
.PHONY: clean all
