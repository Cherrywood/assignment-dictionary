%include "lib.inc"

section .text
global find_word
find_word:
.loop:
	test rsi,rsi
	jz .error
	push rsi
	push rdi
	add rsi, 8
	call string_equals
	pop rdi
	pop rsi
	cmp rax, 1
	je .found
	mov rsi,[rsi]
	jmp .loop
.found:
	mov rax, rsi
	ret
.error:
	xor rax, rax
	ret
