%include "words.inc"
%include "lib.inc"
extern find_word

section .data
size_error: db "Max len key is 255 sembols", 10, 0
key_error: db "value not found", 10, 0
buf: times 255 db 0

section .text
global _start
print_error:
	call string_length
	mov rdx, rax
	mov rax, 1
	mov rsi, rdi
	mov rdi, 2
	syscall
	ret 

_start:
	mov rdi, buf
	mov rsi, 255
	call read_word
	test rax, rax
	jz .size_err
	mov rdi, rax
	mov rsi, last_element
	call find_word
	test rax, rax
	jz .not_found
	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	inc rax
	add rdi,rax
	call print_string
	call print_newline
	call exit
.size_err:
	mov rdi, size_error
	call print_error
	call exit
.not_found:
	mov rdi, key_error
	call print_error
	call exit
